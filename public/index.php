<?php

/**
 * Tiat Framework
 *
 * @package        Tiat/Skeleton
 * @license        BSD-3-Clause
 */

declare( strict_types=1 );

//
use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Skeleton\Bootstrap\SkeletonApplication;
use Jantia\Skeleton\Bootstrap\SkeletonCli;

/**
 * Register the Composer autoloader
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
if(! empty($root = getenv('APPS_ROOT_DIR'))):
	if(! file_exists($file = $root . '/vendor/autoload.php')):
		throw new \Jantia\Skeleton\Exception\RuntimeException('Autoload.php not found from anywhere');
	endif;
	
	//
	require_once $file;
else:
	throw new \Jantia\Skeleton\Exception\InvalidArgumentException("Root path env can't be empty.");
endif;

// Set default params
$params = ['params' => [], 'autoinit' => TRUE, 'autorun' => TRUE, AsiRegisterParams::MONITORING_STATUS->value => TRUE];

// Detect CLI or other
if(str_starts_with(PHP_SAPI, 'cli')):
	( new SkeletonCli(...$params) );
else:
	( new SkeletonApplication(...$params) );
endif;
