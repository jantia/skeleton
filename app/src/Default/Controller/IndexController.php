<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Skeleton\Default\Controller;

//
use Tiat\Mvc\Controller\AbstractActionController;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class IndexController extends AbstractActionController {
	
	/**
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function indexAction() : void {
	
	}
}
