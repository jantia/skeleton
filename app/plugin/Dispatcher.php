<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace SkeletonPlugin;

//
use Jantia\Plugin\Layout\Router\DispatcherPlugin;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class Dispatcher extends DispatcherPlugin {

}
