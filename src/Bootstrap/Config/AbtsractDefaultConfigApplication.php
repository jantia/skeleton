<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Bootstrap\Config;

use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Skeleton\Bootstrap\Register\RegisterDefault;
use Jantia\Skeleton\Exception\RuntimeException;
use Psr\Log\LogLevel;
use Tiat\Config\File\AbstractDefaultConfigFile;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Register\RegisterPluginInterface;
use Tiat\Stdlib\Register\Register;

use function define;
use function defined;

/**
 * Abstract class AbstractDefaultConfigApplication
 * This class serves as a base for applications with default configuration.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbtsractDefaultConfigApplication extends AbstractDefaultConfigFile {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INVALID_REGISTER = 'invalid_register';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INVALID_CONFIG = 'invalid_config';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string CONFIG_SUCCEED = 'config_succeed';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string REGISTER_ENCRYPTION_KEY = 'encryption';
	
	/**
	 * Default Bootstrap interface register. This constant+value is mandatory for application if Register is used.
	 * You can override this by application.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_REGISTER = RegisterDefault::class;
	
	/**
	 * Default Bootstrap register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_BOOTSTRAP_REGISTER_INTERFACE = Register::class;
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_templates = [LogLevel::WARNING => [self::INVALID_REGISTER => "Invalid Register object or class/file doesn't exists."],
	                               LogLevel::INFO => [self::CONFIG_SUCCEED => 'Configuration has been successfully initialized.'],
	                               LogLevel::ERROR => [self::INVALID_CONFIG => "Configuration is not valid and can be initialized."]];
	
	/**
	 * Defines the default register handler.
	 * This method sets the default register handler name as a global constant.
	 * It first checks if the class constant for the default register handler exists.
	 * If it does, it sets the default register handler using the `setRegisterHandlerDefault` method.
	 * It then checks if the default register handler has been successfully set.
	 * If it has, it checks if the global constant for the default register handler
	 * does not exist and defines it using the `define` function.
	 * If the definition fails, it throws a RuntimeException.
	 * If the default register handler is missing, it also throws a RuntimeException.
	 *
	 * @return static the current instance of the class
	 * @throws RuntimeException if the global constant for the default register handler cannot be defined or if the default register handler is missing
	 * @since   3.0.0 First time introduced.
	 */
	protected function _defineDefaultRegisterHandler() : static {
		// Set default register handler name as global constant
		if(( $r = $this->getClassConstant(AsiRegisterParams::DEFAULT_REGISTER->value) ) !== NULL):
			$this->setRegisterHandlerDefault($r, TRUE);
		endif;
		
		//
		if(( $default = $this->getRegisterHandlerDefault() ) !== NULL):
			if(! defined(AsiRegisterParams::DEFAULT_REGISTER->value) &&
			   define(AsiRegisterParams::DEFAULT_REGISTER->value, $default) === FALSE):
				throw new RuntimeException("Can't defined global constant for default register handler name. Application has been stopped.");
			endif;
		else:
			throw new RuntimeException("Default register handler is missing. Application has been stopped.");
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Registers a configuration using a register handler.
	 * This method takes a register handler name or an instance of `RegisterPluginInterface` and `ParametersPluginInterface`,
	 * along with arrays of default configuration values, arguments, and options.
	 * It first checks if the register handler has been set and if the values to be registered are not empty.
	 * It then pulls the register using the register handler's `pullRegister` method with a new instance of the specified register.
	 * After that, it registers the configuration values using the `registerConfigValues` method of the register handler.
	 * If the `setRegisterHandler` parameter is `true`, it sets the register handler using the `setRegisterHandler` method of the class.
	 * Finally, it returns `true` if the registration was successful, or `false` if not.
	 *
	 * @param    (RegisterPluginInterface&ParametersPluginInterface|string)    $register              The register handler name or an instance of `RegisterPluginInterface` and `ParametersPluginInterface`
	 * @param    array                                                         $config
	 * @param    bool                                                          $setRegisterHandler    Whether to set the register handler after registration (optional, default is `false`)
	 * @param    string|null                                                   $name                  The name of the register handler (optional)
	 *
	 * @return bool `true` if the registration was successful, `false` otherwise
	 * @since   3.5.0 First time introduced.
	 */
	protected function _registerConfig(( RegisterPluginInterface&ParametersPluginInterface )|string $register, array $config, bool $setRegisterHandler = FALSE, string $name = NULL) : bool {
		// Pull the register
		if(( $class = $this->_getRegisterHandler($name) ) !== NULL):
			//
			$r = $class::pullRegister(new $register());

			//
			$this->registerConfigValues($r, $config);
			
			//
			if($setRegisterHandler === TRUE):
				$this->setRegisterHandler($class, overwrite:TRUE);
			endif;
			
			//
			return TRUE;
		endif;
		
		//
		return FALSE;
	}
}
