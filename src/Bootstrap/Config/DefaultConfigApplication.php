<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Bootstrap\Config;

//
use Jantia\Asi\Helper\AsiProcessHelperTrait;
use Jantia\Asi\Interface\Bootstrap\AsiBootstrap;
use Jantia\Asi\Interface\Bootstrap\Register\AsiBootstrapRegister;
use Jantia\Asi\Interface\Kernel\AsiKernel;
use Jantia\Asi\Interface\Kernel\Layer\Datalink\AsiKernelDatalink;
use Jantia\Asi\Interface\Kernel\Layer\Network\AsiKernelNetwork;
use Jantia\Asi\Interface\Kernel\Layer\Physical\AsiKernelPhysical;
use Jantia\Asi\Interface\Kernel\Register\AsiKernelRegister;
use Jantia\Asi\Interface\Software\AsiSoftware;
use Jantia\Asi\Interface\Software\Layer\Application\AsiSoftwareApplication;
use Jantia\Asi\Interface\Software\Layer\Presentation\AsiSoftwarePresentation;
use Jantia\Asi\Interface\Software\Layer\Session\AsiSoftwareSession;
use Jantia\Asi\Interface\Software\Register\AsiSoftwareRegister;
use Jantia\Asi\Interface\Transfer\AsiTransfer;
use Jantia\Asi\Interface\Transfer\Register\AsiTransferRegister;
use Jantia\Asi\Register\AsiRegisterModules;
use Jantia\Asi\Register\AsiRegisterParams;
use Jantia\Asi\Std\AsiTypeInterface;
use Jantia\Skeleton\Exception\RuntimeException;
use Jantia\Standard\Asi\Interface\AsiBootstrapInterface;
use Jantia\Standard\Asi\Interface\AsiKernelInterface;
use Jantia\Standard\Asi\Interface\AsiSoftwareInterface;
use Jantia\Standard\Asi\Interface\AsiTransferInterface;
use Jantia\Standard\Asi\Layer\Kernel\AsiKernelDatalinkInterface;
use Jantia\Standard\Asi\Layer\Kernel\AsiKernelNetworkInterface;
use Jantia\Standard\Asi\Layer\Kernel\AsiKernelPhysicalInterface;
use Jantia\Standard\Asi\Layer\Software\AsiSoftwareApplicationInterface;
use Jantia\Standard\Asi\Layer\Software\AsiSoftwarePresentationInterface;
use Jantia\Standard\Asi\Layer\Software\AsiSoftwareSessionInterface;
use Jantia\Standard\Platform\PlatformEnv;
use Psr\Log\LoggerInterface;
use ReflectionClass;
use Tiat\Collection\Crypt\CryptoBox;
use Tiat\Collection\Crypt\CryptoBoxParams;
use Tiat\Collection\Crypt\Register\CryptoBoxRegister;
use Tiat\Collection\Envelope\Envelope;
use Tiat\Collection\Envelope\EnvelopeParams;
use Tiat\Collection\Envelope\Register\EnvelopeRegister;
use Tiat\Collection\MessageBus\MessageBus;
use Tiat\Router\Router;
use Tiat\Router\Router\Type\Plugin\PluginLiteral;
use Tiat\Router\Std\RouterType;
use Tiat\Standard\Parameters\ParametersPluginInterface;
use Tiat\Standard\Router\RouterInterface;
use Tiat\Stdlib\Register\RegisterContainer;
use Tiat\Standard\Register\RegisterInterface;

use function get_class;
use function sprintf;

/**
 * Default config for the application. You can extend this class each method with your own custom settings.
 * As default add ..\config\DefaultConfig.php to your application directory and extend this class.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class DefaultConfigApplication extends AbtsractDefaultConfigApplication {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiProcessHelperTrait;
	
	/**
	 * Default CryptoBox register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_CRYPTOBOX_REGISTER_INTERFACE = CryptoBoxRegister::class;
	
	/**
	 * Default Envelope register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_ENVELOPE_REGISTER_INTERFACE = EnvelopeRegister::class;
	
	/**
	 * Default Bootstrat interface register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_BOOTSTRAP_REGISTER_INTERFACE = AsiBootstrapRegister::class;
	
	/**
	 * Default Kernel interface register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_KERNEL_REGISTER_INTERFACE = AsiKernelRegister::class;
	
	/**
	 * Default Software interface register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_SOFTWARE_REGISTER_INTERFACE = AsiSoftwareRegister::class;
	
	/**
	 * Default Transfer interface register
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string DEFAULT_TRANSFER_REGISTER_INTERFACE = AsiTransferRegister::class;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string MODULE_NAME_BOOTSTRAP = 'asi.bootstrap';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string MODULE_NAME_TRANSFER = 'asi.transfer';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string MODULE_NAME_SOFTWARE = 'asi.software';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string MODULE_NAME_KERNEL = 'asi.kernel';
	
	/**
	 * Define cert path
	 *
	 * @since   3.0.0 First time introduced.
	 */
	protected const string CRYPTO_CERT_PATH = '';
	
	/**
	 * Default CIPHER for the application
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string CRYPTO_DEFAULT_CIPHER = 'aes-256-gcm';
	
	/**
	 * Default HASH ALGO for the application
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public const string CRYPTO_DEFAULT_HASH = 'sha256';
	
	/**
	 * Application monitoring
	 *
	 * @since   3.0.0 First time introduced.
	 */
	#public const bool MONITORING_STATUS = FALSE;
	
	/**
	 * If you want to use Pipelines then recommend options are ['autoinit'=>true, 'autorun'=>false]
	 *
	 * @var array|true[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_defaultArgs = ['autoinit' => false, 'autorun' => FALSE];
	
	/**
	 * @param ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		//
		$this->setMessageTemplates($this->_templates);
		
		//
		$this->_defineDefaultRegisterHandler();
		
		//
		parent::__construct($params, $autoinit, $autorun, $args);
		
		// If autoinit is TRUE then prevent double execution
		if($autoinit === FALSE && $this->getAutoinit() === TRUE):
			$this->init(...$args);
		endif;
		
		// If autorun is TRUE then prevent double execution
		if($autorun === FALSE && $this->getAutorun() === TRUE):
			$this->run(...$args);
		endif;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function shutdown(...$args) : void {
		//
		$class    = $this->getRegisterHandler(get_class($this));
		$register = $this->getRegisterHandlerDefault();
		if(( $r = $class::pullRegister(new $register()) ) !== NULL):
			if($r instanceof ParametersPluginInterface):
				$name = $r->getParam(PlatformEnv::ENV_APP_NAME->value);
				$this->setMessage(self::CONFIG_SUCCEED,
			                  ['description' => sprintf("Application %s config has been initialized without errors.",
			                                            $name)]);
			endif;
		else:
			throw new RuntimeException("Application don't have registered Register interface.");
		endif;
		
		// Parent shutdown will save all messages with valid Psr\Log\LoggerInterface
		parent::shutdown($args);
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	public function registerConfig(...$args) : static {
		// Run all interface config sections. Order doesn't matter because it's defined inside config.
		$this->_initInterfaceBootstrap(...$args)
		     ->_initInterfaceTransfer(...$args)
		     ->_initInterfaceKernel(...$args)
		     ->_initInterfaceSoftware(...$args);
		
		//
		return $this;
	}
	
	/**
	 * Init Software interface config. Use RegisterContainer for register config.
	 *
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _initInterfaceSoftware(...$args) : static {
		// Define default values
		$settings = [
			AsiTypeInterface::SOFTWARE->value => [
				AsiRegisterParams::NAME->value => self::MODULE_NAME_SOFTWARE,
				AsiRegisterParams::MONITORING_STATUS->value => $this->getClassConstant(AsiRegisterParams::MONITORING_STATUS->value),
			],
			AsiRegisterParams::PROCESS->value => [
	            AsiRegisterParams::HANDLER->value => AsiSoftware::class,
	            AsiRegisterParams::CONTRACT->value => AsiSoftwareInterface::class,
	            AsiRegisterParams::PARAMS->value => $this->_defaultArgs?? [],
			],
			AsiRegisterParams::NEXT_LAYER->value => [
				...$this->_setConfigSoftwareLayerSession(...$args),
				...$this->_setConfigSoftwareLayerApplication(...$args),
				...$this->_setConfigSoftwareLayerPresentation(...$args)
			],
			AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
                AsiRegisterParams::PIPELINE_METHOD->value => 'run',
            ]
		];
		
		// Define RegisterContainer
		$v3 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v1 = (new RegisterContainer(AsiRegisterParams::NEXT_LAYER->value))->setParams($settings[AsiRegisterParams::NEXT_LAYER->value]);
		$e = (new RegisterContainer(AsiTypeInterface::SOFTWARE->value))->setParams($settings[AsiTypeInterface::SOFTWARE->value])
		                                                        ->addContainer($v1)
		                                                        ->addContainer($v2)
		                                                        ->addContainer($v3);
		
		// Register this as a DEFAULT register handler for application general settings
		$config = [AsiTypeInterface::SOFTWARE->value => $e];
		
		// Register settings
		if($this->_registerConfig($r = self::DEFAULT_SOFTWARE_REGISTER_INTERFACE, $config,   TRUE) === FALSE):
			// Save to LOGS
			$msg   = sprintf("Register %s can not be initialized.", $r);
			$debug = ['config' => $config];
			$this->setMessage(self::INVALID_CONFIG, ['message' => $msg, 'debug' => $debug]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Session layer for Software interface. Use RegisterContainer for register config.
	 *
	 * @param ...$args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigSoftwareLayerSession(...$args) : array {
		$name = 'asi.software.session';
		$settings = [
			$name => [
				AsiRegisterParams::NAME->value => $name,
			],
			AsiRegisterParams::PROCESS->value => [
		        AsiRegisterParams::HANDLER->value => AsiSoftwareSession::class,
		        AsiRegisterParams::CONTRACT->value => AsiSoftwareSessionInterface::class,
		        AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
	        ],
			AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
				AsiRegisterParams::PIPELINE_METHOD->value => 'run',
			]
		];
		
		//
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$e = (new RegisterContainer($name))->setParams($settings[$name])->addContainer($v1)->addContainer($v2);
		
		//
		return [$name => $e];
	}
	
	/**
	 * Application layer for Software interface. Use RegisterContainer for register config.
	 *
	 * @param ...$args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigSoftwareLayerApplication(...$args) : array {
		//
		$name = 'asi.software.application';
		$settings = [
			$name => [
				AsiRegisterParams::NAME->value => $name,
			],
			AsiRegisterParams::PROCESS->value => [
		        AsiRegisterParams::HANDLER->value => AsiSoftwareApplication::class,
		        AsiRegisterParams::CONTRACT->value => AsiSoftwareApplicationInterface::class,
		        AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
			],
			AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
				AsiRegisterParams::PIPELINE_METHOD->value => 'run',
			]
		];
		
		//
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$e = (new RegisterContainer($name))->setParams($settings[$name])->addContainer($v1)->addContainer($v2);
		
		//
		return [$name => $e];
	}
	
	/**
	 * Presentation layer for Software interface. Use RegisterContainer for register config.
	 *
	 * @param ...$args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigSoftwareLayerPresentation(...$args) : array {
		//
		$name = 'asi.software.presentation';
		$settings = [
			$name => [
				AsiRegisterParams::NAME->value => $name,
			],
			AsiRegisterParams::PROCESS->value => [
		        AsiRegisterParams::HANDLER->value => AsiSoftwarePresentation::class,
		        AsiRegisterParams::CONTRACT->value => AsiSoftwarePresentationInterface::class,
		        AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
			],
			AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
	            AsiRegisterParams::PIPELINE_METHOD->value => 'run',
	        ],
		];
		
		//
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$e = (new RegisterContainer($name))->setParams($settings[$name])->addContainer($v1)->addContainer($v2);
		
		//
		return [$name => $e];
	}
	
	
	
	/**
	 * Get the Register instance which handles all sub-registers
	 * Default handler is Tiat\Stdlib\Register\Register()
	 *
	 * @param    string|NULL    $name
	 *
	 * @return null|RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected function _getRegisterHandler(string $name = NULL) : ?RegisterInterface {
		return $this->getRegisterHandler($name ?? get_class($this));
	}

	/**
	 * Init Kernel interface config
	 *
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _initInterfaceKernel(...$args) : static {
		// Define default values
        $settings = [
	        AsiTypeInterface::KERNEL->value => [
	            AsiRegisterParams::NAME->value => self::MODULE_NAME_KERNEL,
                AsiRegisterParams::MONITORING_STATUS->value => $this->getClassConstant(AsiRegisterParams::MONITORING_STATUS->value),
            ],
            AsiRegisterParams::PROCESS->value => [
                AsiRegisterParams::HANDLER->value => AsiKernel::class,
                AsiRegisterParams::CONTRACT->value => AsiKernelInterface::class,
                AsiRegisterParams::PARAMS->value => $this->_defaultArgs?? [],
            ],
            AsiRegisterParams::NEXT_LAYER->value => [
				...$this->_setConfigKernelLayerDatalink(...$args),
				...$this->_setConfigKernelLayerNetwork(...$args),
				...$this->_setConfigKernelLayerPhysical(...$args)
            ],
	        AsiRegisterParams::PIPELINE->value => [
                AsiRegisterParams::PIPELINE_USE->value => TRUE,
                AsiRegisterParams::PIPELINE_METHOD->value => 'run',
            ]
        ];
        
        // Define RegisterContainer
		$v3 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
        $v1 = (new RegisterContainer(AsiRegisterParams::NEXT_LAYER->value))->setParams($settings[AsiRegisterParams::NEXT_LAYER->value]);
		$e = (new RegisterContainer(AsiTypeInterface::KERNEL->value))->setParams($settings[AsiTypeInterface::KERNEL->value])
		                                                             ->addContainer($v1)->addContainer($v2)->addContainer($v3);
		
		//
		$config = [AsiTypeInterface::KERNEL->value => $e];
		
		// Register settings
		if($this->_registerConfig($r = self::DEFAULT_KERNEL_REGISTER_INTERFACE, $config,   TRUE) ===
		   FALSE):
			// Save to LOGS
			$msg   = sprintf("Register %s can not be initialized.", $r);
			$debug = ['config' => $config];
			$this->setMessage(self::INVALID_CONFIG, ['message' => $msg, 'debug' => $debug]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Datalink layer for Kernel interface
	 *
	 * @param ...$args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigKernelLayerDatalink(...$args) : array {
		//
		$name = 'asi.kernel.datalink';
		$settings = [
			$name => [
				AsiRegisterParams::NAME->value => $name,
			],
			AsiRegisterParams::PROCESS->value => [
		        AsiRegisterParams::HANDLER->value => AsiKernelDatalink::class,
		        AsiRegisterParams::CONTRACT->value => AsiKernelDatalinkInterface::class,
		        AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
			],
	        AsiRegisterParams::SETTINGS->value => [
				AsiRegisterParams::VARIABLES_ORDER->value => 'gpcfh',
	            AsiRegisterParams::VALIDATORS->value => []
	        ],
	        AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
	            AsiRegisterParams::PIPELINE_METHOD->value => 'run',
	        ]
		];
		
		//
		$v1 = (new RegisterContainer(AsiRegisterParams::SETTINGS->value))->setParams($settings[AsiRegisterParams::SETTINGS->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v3 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value])->addContainer($v1);
		$e = (new RegisterContainer($name))->setParams($settings[$name])->addContainer($v2)->addContainer($v3);

		//
		return [$name => $e];
	}
	
	/**
	 * Network layer for Kernel interface
	 *
	 * @param ...$args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigKernelLayerNetwork(...$args) : array {
		//
		$name = 'asi.kernel.network';
		$settings = [
			$name => [
				AsiRegisterParams::NAME->value => $name,
			],
			AsiRegisterParams::PROCESS->value => [
		        AsiRegisterParams::HANDLER->value => AsiKernelNetwork::class,
		        AsiRegisterParams::CONTRACT->value => AsiKernelNetworkInterface::class,
		        AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
			],
	        AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
                AsiRegisterParams::PIPELINE_METHOD->value => 'run',
	        ],
		];
		
		//
		$v2 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$e = (new RegisterContainer($name))->setParams($settings[$name])->addContainer($v1)->addContainer($v2);
		
		//
		return [$name => $e];
	}
	
	/**
	 * Physical layer for Kernel interface. Use RegisterContainer to add sub-registers.
	 *
	 * @param ...$args
	 *
	 * @return array
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setConfigKernelLayerPhysical(...$args) : array {
		//
		$name = 'asi.kernel.physical';
		$settings = [
			$name => [
				AsiRegisterParams::NAME->value => $name,
			],
			AsiRegisterParams::PROCESS->value => [
			    AsiRegisterParams::HANDLER->value => AsiKernelPhysical::class,
			    AsiRegisterParams::CONTRACT->value => AsiKernelPhysicalInterface::class,
			    AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
			],
		    AsiRegisterParams::PIPELINE->value => [
				AsiRegisterParams::PIPELINE_USE->value => TRUE,
		        AsiRegisterParams::PIPELINE_METHOD->value => 'run',
		    ],
		];
		
		//
		$v2 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$e = (new RegisterContainer($name))->setParams($settings[$name])->addContainer($v1)->addContainer($v2);
		
		//
		return [$name => $e];
	}
	
	/**
	 * Init Transfer interface config. Use RegisterContainer to add sub-registers.
	 *
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _initInterfaceTransfer(...$args) : static {
		// Define default values
		$settings = [
			AsiTypeInterface::TRANSFER->value => [
	            AsiRegisterParams::NAME->value => self::MODULE_NAME_TRANSFER,
			],
			AsiRegisterParams::PROCESS->value => [
	            AsiRegisterParams::HANDLER->value => AsiTransfer::class,
	            AsiRegisterParams::CONTRACT->value => AsiTransferInterface::class,
	            AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
	            AsiRegisterParams::MONITORING_STATUS->value => $this->getClassConstant(AsiRegisterParams::MONITORING_STATUS->value),
	        ],
	        AsiRegisterParams::PIPELINE->value => [
	            AsiRegisterParams::PIPELINE_USE->value => true,
	            AsiRegisterParams::PIPELINE_METHOD->value => 'run',
	        ],
		];
		
		// Get & add router config
	    $v6 = (new RegisterContainer(AsiRegisterParams::SETTINGS->value))->addContainer($this->_getRouterConfig());
		
		//
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v7 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value])->addContainer($v6);
	    $e = (new RegisterContainer(AsiTypeInterface::TRANSFER->value))->setParams($settings[AsiTypeInterface::TRANSFER->value])->addContainer($v1)->addContainer($v7);
	
		//
	    $config = [AsiTypeInterface::TRANSFER->value => $e];
		
		// Register settings
		if($this->_registerConfig($r = self::DEFAULT_TRANSFER_REGISTER_INTERFACE, $config,   TRUE) === FALSE):
			// Save to LOGS
			$msg   = sprintf("Register %s can not be initialized.", $r);
			$debug = ['config' => $config];
			$this->setMessage(self::INVALID_CONFIG, ['message' => $msg, 'debug' => $debug]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @return RegisterContainer
	 */
	protected function _getRouterConfig() : RegisterContainer {
		//
		$settings = [
	        AsiRegisterParams::SETTINGS->value => [
	            AsiRegisterParams::LANGUAGE_SETTINGS->value => ['path' => 'after', 'default' => 'en'],
	        ],
	        AsiRegisterModules::ROUTER->value => [
	            AsiRegisterParams::HANDLER->value => Router::class,
	            AsiRegisterParams::CONTRACT->value => RouterInterface::class,
	            AsiRegisterParams::PARAMS->value => $this->_defaultArgs,
	        ],
		];
		
		// Define Router model
		$v4 = ($v5 = new RegisterContainer(AsiRegisterParams::SETTINGS->value))->addContainer($this->_getRouterModel());
		
		// Add router language settings if defined
		if(($lang = $this->_getRouterLanguageSettings()) !== null):
			$v5->setParams($lang);
		endif;
		
		// Return Router config container
		return (new RegisterContainer(AsiRegisterModules::ROUTER->value))->setParams($settings[AsiRegisterModules::ROUTER->value])->addContainer($v4);
	}
	
	/**
	 * @return null|array[]
	 */
	protected function _getRouterLanguageSettings() : ?array {
		$settings = [
            AsiRegisterParams::LANGUAGE_SETTINGS->value => ['path' => 'after', 'default' => 'en'],
        ];
		
		return $settings??null;
	}
	
	/**
	 * @return RegisterContainer
	 */
	protected function _getRouterModel() : RegisterContainer {
		$settings = [
			AsiRegisterParams::SETTINGS->value => [
	            AsiRegisterParams::ROUTER_PLUGIN->value => PluginLiteral::class,
	        ],
	        AsiRegisterParams::ROUTER_MODEL->value => [
	            AsiRegisterParams::ROUTER_TYPE->value => RouterType::TREE,
	            AsiRegisterParams::CONTRACT->value => Router\Type\RouterTypeInterface::class,
	            AsiRegisterParams::PARAMS->value => $this->_defaultArgs ?? [],
	        ],
		];
		
		//
		$v1 = (new RegisterContainer(AsiRegisterParams::SETTINGS->value))->setParams($settings[AsiRegisterParams::SETTINGS->value]);
	    return (new RegisterContainer(AsiRegisterParams::ROUTER_MODEL->value))->setParams($settings[AsiRegisterParams::ROUTER_MODEL->value])->addContainer($v1);
	}
	
	/**
	 * Init Bootstrap interface config. Use RegisterContainer to add sub-registers.
	 *
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 */
	protected function _initInterfaceBootstrap(...$args) : static {
		// Set general settings at first because the order is important
		$this->_setConfigGeneral(...$args)->_setConfigEncryption(...$args);
		
		// Define default values
		$settings = [
			AsiTypeInterface::BOOTSTRAP->value => [
				AsiRegisterParams::NAME->value => self::MODULE_NAME_BOOTSTRAP,
                AsiRegisterParams::MONITORING_STATUS->value => $this->getClassConstant(AsiRegisterParams::MONITORING_STATUS->value),
			],
			// This section will define which interface will be run next.
            AsiRegisterParams::PROCESS->value => [
	            AsiRegisterParams::HANDLER->value => AsiBootstrap::class,
	            AsiRegisterParams::CONTRACT->value => AsiBootstrapInterface::class,
	            AsiRegisterParams::PARAMS->value => $this->_defaultArgs ??[]
            ],
			// This section will define the interface(s) RegisterName which will be run inside this interface.
            AsiRegisterParams::NEXT_INTERFACE->value => [
	            AsiTypeInterface::KERNEL->value => self::DEFAULT_KERNEL_REGISTER_INTERFACE,
	            AsiTypeInterface::TRANSFER->value => self::DEFAULT_TRANSFER_REGISTER_INTERFACE,
	            AsiTypeInterface::SOFTWARE->value => self::DEFAULT_SOFTWARE_REGISTER_INTERFACE,
            ],
	        AsiRegisterParams::PIPELINE->value => [
	            AsiRegisterParams::PIPELINE_USE->value => TRUE,
	            AsiRegisterParams::PIPELINE_METHOD->value => 'run',
	        ]
		];

		//
		$v3 = (new RegisterContainer(AsiRegisterParams::PROCESS->value))->setParams($settings[AsiRegisterParams::PROCESS->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::NEXT_INTERFACE->value))->setParams($settings[AsiRegisterParams::NEXT_INTERFACE->value]);
		$v1 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$e = (new RegisterContainer(AsiTypeInterface::BOOTSTRAP->value))->setParams($settings[AsiTypeInterface::BOOTSTRAP->value])
		                                                         ->addContainer($v1)->addContainer($v2)->addContainer($v3);
		
		// Merge all
		$config = [AsiTypeInterface::BOOTSTRAP->value => $e];
		
		// Register settings
		if($this->_registerConfig($r = self::DEFAULT_BOOTSTRAP_REGISTER_INTERFACE, $config,   TRUE) ===
		   FALSE):
			// Save to LOGS
			$msg   = sprintf("Register %s can not be initialized.", $r);
			$debug = ['config' => $config];
			$this->setMessage(self::INVALID_CONFIG, ['message' => $msg, 'debug' => $debug]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Encryption settings for global application. Use RegisterContainer to add sub-registers.
	 *
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 * @see     https://jantia.io/tiat/cryptobox
	 */
	protected function _setConfigEncryption(...$args) : static {
		//
		$name = self::REGISTER_ENCRYPTION_KEY;
		$cryptobox = (new ReflectionClass(CryptoBox::class))->getShortName();
		$envelope = (new ReflectionClass(Envelope::class))->getShortName();
		
		//
		$settings = [
			$envelope => [
				EnvelopeParams::ENVELOPE_PARAM_STATUS_CRYPT->value => TRUE,
                EnvelopeParams::ENVELOPE_PARAM_CRYPTO_EXTENSION->value => new CryptoBox($this->getClassConstant(AsiRegisterParams::CRYPTO_CERT_PATH->value)),
			]
		];
		
		$v2 = (new RegisterContainer($envelope))->setParams($settings[$envelope]);
		$e = (new RegisterContainer($name))->addContainer($this->_setCryptoBox($cryptobox))->addContainer($v2);
		
		//
		$config = [$name => $e];
		
		//
		if($this->_registerConfig(self::DEFAULT_ENVELOPE_REGISTER_INTERFACE, $config, TRUE) === FALSE):
			// Save to LOGS
			$msg = sprintf("Register %s can not be initialized on application %s.",self::DEFAULT_ENVELOPE_REGISTER_INTERFACE, $name);
			$this->setMessage(self::INVALID_CONFIG,
			                  ['message' => $msg, 'debug' => ['config' => $config]]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * @param    string    $name
	 *
	 * @return null|RegisterContainer
	 * @since   3.0.0 First time introduced.
	 */
	protected function _setCryptoBox(string $name) : ?RegisterContainer {
		//
		$settings = [
	        $name => [
	        // CryptoBox: use OpenSSL as default (instead of Sodium)
	        CryptoBoxParams::CRYPTO_PARAM_EXTENSION->value => CryptoBoxParams::CRYPTO_EXTENSION_OPENSSL,
	        
	        // CryptoBox: use symmetric method as default
	        CryptoBoxParams::CRYPTO_PARAM_MODE->value => CryptoBoxParams::CRYPTO_MODE_SYMMETRIC,
	        
	        // CryptoBox: default hash & cipher
	        CryptoBoxParams::CRYPTO_HASH->value => self::CRYPTO_DEFAULT_HASH,
	        CryptoBoxParams::CRYPTO_CIPHER->value => self::CRYPTO_DEFAULT_CIPHER,
	        
	        // CryptoBox: certifications path is needed for CryptoBox
	        // Link to certification files or create them with CLI
	        // Raise an error if PATH has already defined somehow
	        // so the path should be always same and should NOT be replaced with new value
	        CryptoBoxParams::CRYPTO_CERT_PATH->value => $this->getClassConstant(AsiRegisterParams::CRYPTO_CERT_PATH->value),
	        
	        // Define public and private keys
	        CryptoBoxParams::CRYPTO_PEM_KEY_PUBLIC->value => 'public_key.pem',
	        CryptoBoxParams::CRYPTO_PEM_KEY_PRIVATE->value => 'private_key.pem',
	        CryptoBoxParams::CRYPTO_PEM_PASSWORD->value => null
	        ]
		];
		
		return (new RegisterContainer($name))->setParams($settings[$name]);
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 * @see     https://jantia.io/tiat/bootstrap
	 */
	protected function _setConfigGeneral(...$args) : static {
		// Pre-define values
		$name = $this->getApplicationName();

		// Message Bus interface for transferring DTO's inside the application
		$bus = new MessageBus();
		
		// Define default values
		$settings = [
            $name => [
				AsiRegisterParams::APPLICATION_NAME->value => $name,
				AsiRegisterParams::ERROR_HANDLER->value => $this->_defineErrorHandler($name),
			    AsiRegisterParams::MESSAGE_BUS->value => $bus,
				AsiRegisterParams::LOG_HANDLER->value => $this->_defineLogger($name),
			    AsiRegisterParams::MONITORING_STATUS->value => $this->getClassConstant(AsiRegisterParams::MONITORING_STATUS->value) ?? FALSE
            ],
            AsiRegisterParams::NEXT_INTERFACE->value => [
                AsiTypeInterface::BOOTSTRAP->value => self::DEFAULT_BOOTSTRAP_REGISTER_INTERFACE,
            ],
            // Use the Pipeline
            AsiRegisterParams::PIPELINE->value => [
                AsiRegisterParams::PIPELINE_USE->value => TRUE,
                AsiRegisterParams::PIPELINE_METHOD->value => 'run',
            ],
            AsiRegisterParams::REGISTER->value => [
	            AsiTypeInterface::BOOTSTRAP->value => self::DEFAULT_BOOTSTRAP_REGISTER_INTERFACE,
	            AsiTypeInterface::KERNEL->value => self::DEFAULT_KERNEL_REGISTER_INTERFACE,
	            AsiTypeInterface::SOFTWARE->value => self::DEFAULT_SOFTWARE_REGISTER_INTERFACE,
	            AsiTypeInterface::TRANSFER->value => self::DEFAULT_TRANSFER_REGISTER_INTERFACE
            ]
        ];
		
		//
		$v3 = (new RegisterContainer(AsiRegisterParams::REGISTER->value))->setParams($settings[AsiRegisterParams::REGISTER->value]);
		$v2 = (new RegisterContainer(AsiRegisterParams::PIPELINE->value))->setParams($settings[AsiRegisterParams::PIPELINE->value]);
		$v1 = (new RegisterContainer(AsiRegisterParams::NEXT_INTERFACE->value))->setParams($settings[AsiRegisterParams::NEXT_INTERFACE->value]);
		$e = (new RegisterContainer($name))->setParams($settings[(string)$name])
		                                   ->addContainer($v1)->addContainer($v2)->addContainer($v3);
		
		//
		$config = [$name=>$e];
		
		// Register this as a DEFAULT register handler for application general settings
		if($this->_registerConfig(self::DEFAULT_REGISTER, $config, TRUE) === FALSE):
			// Save to LOGS
			$msg = sprintf("Register %s can not be initialized on application %s.",
			               self::DEFAULT_REGISTER, $name);
			$this->setMessage(self::INVALID_CONFIG,
			                  ['message' => $msg, 'debug' => ['config' => $config]]);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Define the logger. Overwrite this method to define your own logger. You can also connect this with $this->_defineErrorHandler().
	 *
	 * @return LoggerInterface|null The logger instance or null if not defined.
	 * @since 3.0.0 First time introduced.
	 */
	protected function _defineLogger(string $name) : ?LoggerInterface {
		return null;
	}
	
	/**
	 * Define error handler. Overwrite this method to define your own logger.
	 *
	 * @param    string    $name    The name of the error handler.
	 *
	 * @return LoggerInterface|null The defined error handler.
	 * @since   3.0.0 First time introduced.
	 */
	protected function _defineErrorHandler(string $name) : ?LoggerInterface {
		return null;
	}
}
