<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Bootstrap;

//
use Jantia\Asi\Helper\AsiConfigHelperTrait;
use Jantia\Asi\Helper\AsiProcessHelperTrait;
use Jantia\Asi\Interface\AbstractAsi;
use Psr\Log\LogLevel;
use Tiat\Config\Loader\ConfigLoader;
use Tiat\Stdlib\Loader\ClassLoaderTrait;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
abstract class AbstractSkeletonBootstrap extends AbstractAsi implements SkeletonInterface {
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiProcessHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string ALERT_APPLICATION_PROCESS_FAIL = 'application_process_fail';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string INFO_APPLICATION_PROCESS_OK = 'application_process_ok';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string WARNING_APPLICATION_PROCESS_RERUN = 'application_process_rerun';
	
	/**
	 * @var array|string[][]
	 * @since   3.0.0 First time introduced.
	 */
	private array $_templates = [LogLevel::ALERT => [self::ALERT_APPLICATION_PROCESS_FAIL => 'Application process has been failed.'],
	                             LogLevel::INFO => [self::INFO_APPLICATION_PROCESS_OK => 'Application has been successfully processed.'],
	                             LogLevel::WARNING => [self::WARNING_APPLICATION_PROCESS_RERUN => "Application process has already run and re-run has been forbidden."]];
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use AsiConfigHelperTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ClassLoaderTrait;
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	use ConfigLoader;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		//
		$this->setMessageTemplates($this->_templates);
		
		//
		parent::__construct($params, $autoinit, $autorun, ...$args);
		
		//
		$this->_defineAutostart($autoinit, $autorun, ...$args);
	}
}
