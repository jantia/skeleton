<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Bootstrap;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class SkeletonCli extends AbstractSkeletonBootstrap {
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 */
	public function __construct(iterable $params, bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		//
		parent::__construct($autoinit, $autorun, ...$args);
		echo __METHOD__.'<br>';
		phpinfo();
		exit;
		
		$path = realpath(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
		if(is_string($path) && __FILE__ !== $path && is_file($path)):
			echo 'Is file<br>';
			#return FALSE;
		else:
			echo 'Not file<br>';
		endif;
	}
}
