<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Bootstrap;

//
use Jantia\Skeleton\Bootstrap\Config\DefaultConfigApplication;
use Jantia\Skeleton\Exception\InvalidArgumentException;
use Jantia\Skeleton\Exception\RuntimeException;
use Jantia\Standard\Platform\PlatformEnv;
use Tiat\Standard\Register\RegisterInterface;
use Tiat\Stdlib\Register\RegisterHandler;

use function class_exists;
use function get_class;
use function getenv;
use function sprintf;

/**
 * Non-cli version of the application which will be run by Skeleton Bootstrap. This bootstrap will use
 * Tiat\Asi as default system and framework. You can extend and use other frameworks also but please read the docs.
 *
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 * @see     https://jantia.io/tiat/skeleton
 */
class SkeletonApplication extends AbstractSkeletonBootstrap {
	
	/**
	 * For security reasons, this constant/file is FINAL
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_CONFIG_BOOTSTRAP = DefaultConfigApplication::class;
	
	/**
	 * For security reasons, this constant/file is FINAL
	 *
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_CONFIG_BOOTSTRAP_APPLICATION = 'ApplicationConfig\DefaultConfig';
	
	/**
	 * @since   3.0.0 First time introduced.
	 */
	final public const string DEFAULT_REGISTER_HANDLER = RegisterHandler::class;
	
	/**
	 * @var array|string[]
	 * @since   3.0.0 First time introduced.
	 */
	public array $_envRequired = [PlatformEnv::ENV_APPS_ROOT_DIR->value, PlatformEnv::ENV_APPS_RUNMODE->value,
	                              PlatformEnv::ENV_APPS_DEBUG->value, PlatformEnv::ENV_LOG_DEFAULT->value,
	                              PlatformEnv::ENV_LOG_ERROR->value, PlatformEnv::ENV_LOG_LEVEL->value,
	                              PlatformEnv::ENV_NAMESPACE->value, PlatformEnv::ENV_APP_NAME->value,
	                              PlatformEnv::ENV_PLATFORM_DEFAULT->value];
	
	/**
	 * @var array|false[]
	 * @since   3.0.0 First time introduced.
	 */
	protected array $_defaultArgs = ['autoinit' => TRUE, 'autorun' => FALSE];
	
	/**
	 * @var RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	protected RegisterInterface $_registerApplication;
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args) {
		//
		parent::__construct($params, FALSE, FALSE, ...$args);
		
		// Set trace id
		$this->setPlatform();
		
		//
		$this->_defineAutostart($autoinit, $autorun, ...$args);
	}
	
	/**
	 * @param ...$args
	 *
	 * @return SkeletonApplication
	 * @since   3.0.0 First time introduced.
	 */
	public function run(...$args) : static {
		// Check the parent
		parent::run(...$args);
		
		// Process this section only once
		if($this->isInitCompleted() === TRUE && $this->hasRun() === FALSE):
			// If application has not been run already then start the process
			if(( ! isset($args['hasRun']) || $args['hasRun'] === FALSE ) &&
			   ( $register = $this->getRegisterApplication() ) !== NULL):
				if($register instanceof RegisterInterface):
					// Get default args
					$args = $this->_defaultArgs;
					
					// Process the application process
					$this->processRegister($register, ...$args);
				endif;
				
				// Mark this process at executed
				$this->setRun(TRUE);
			else:
				//
				$this->setMessage(self::WARNING_APPLICATION_PROCESS_RERUN);
			endif;
			
			//
			return $this;
		endif;
		
		//
		throw new RuntimeException(sprintf("%s can't be executed and application has been stopped.", __METHOD__));
	}
	
	/**
	 * @return null|RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterApplication() : ?RegisterInterface {
		return $this->_registerApplication ?? NULL;
	}
	
	/**
	 * @param    RegisterInterface    $register
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterApplication(RegisterInterface $register) : void {
		$this->_registerApplication = $register;
	}
	
	/**
	 * @param ...$args
	 *
	 * @return $this
	 * @since   3.0.0 First time introduced.
	 * @see     DefaultConfigApplication
	 */
	public function init(...$args) : static {
		// Check the parent
		parent::init(...$args);
		
		// Process this section only once
		if($this->isInitCompleted() === TRUE):
			// Reset init status
			$this->setInitCompleted(FALSE);
			
			// Check mandatory env
			$this->_checkEnv();
			
			//
			if($this->checkConfigFile() === FALSE):
				// Use (Bootstrap) application config file if exists or otherwise Tiat\Skeleton default config file
				if(class_exists($file = self::DEFAULT_CONFIG_BOOTSTRAP_APPLICATION) === FALSE):
					$file = self::DEFAULT_CONFIG_BOOTSTRAP;
				endif;

				//
				$this->_setConfigFile($file);
				
				//
				$this->setConfigFile($this->_getConfigFile());
			endif;
			
			//
			if(! empty($file = $this->getConfigFile())):
				$this->getSettings($file);
			endif;
			
			// Define the main application process (interface) if Register is not set.
			if($this->getRegisterApplication() === NULL):
				$this->_resolveRegister();
			endif;
			
			// Set init ok
			$this->setInitCompleted(TRUE);
		endif;
		
		//
		return $this;
	}
	
	/**
	 * Check that required ENV params have been set for the application
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	protected function _checkEnv() : void {
		foreach($this->_envRequired as $val):
			if(! isset($_ENV[$val]) && ! isset($_SERVER[$val]) && empty(getenv($val))):
				$msg = sprintf("Env %s is required for Tiat Platform", $val);
				throw new InvalidArgumentException($msg);
			endif;
		endforeach;
	}
	
	/**
	 * This will resolve the default register and return the instance of it.
	 *
	 * @since   3.0.0 First time introduced.
	 */
	protected function _resolveRegister() : void {
		//
		$file    = $this->_getConfigFile();
		$handler = self::DEFAULT_REGISTER_HANDLER;
		$default = new $handler();
		
		// Return register instance.
		if(( $class = $default->getRegisterHandler(get_class($default)) ) !== NULL):
			$r = $file::DEFAULT_REGISTER ?? NULL;
			
			// Get old instance (if defined already) or the new one which has registered
			if(( $register = $class::pullRegister(new $r()) ) && $register instanceof RegisterInterface):
				$this->setRegisterApplication($register);
			else:
				throw new RuntimeException(sprintf("Resolved register is not an instance of %s",
				                                   RegisterInterface::class));
			endif;
		endif;
	}
}
