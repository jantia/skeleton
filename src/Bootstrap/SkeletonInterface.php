<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Bootstrap;

//
use Tiat\Standard\Loader\ClassLoaderInterface;
use Tiat\Standard\Register\RegisterInterface;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
interface SkeletonInterface extends ClassLoaderInterface {
	
	/**
	 * @param    iterable    $params
	 * @param    bool        $autoinit
	 * @param    bool        $autorun
	 * @param                ...$args
	 *
	 * @since   3.0.0 First time introduced.
	 */
	public function __construct(iterable $params = [], bool $autoinit = TRUE, bool $autorun = TRUE, ...$args);
	
	/**
	 * Get named constant from class
	 *
	 * @param    string         $name
	 * @param    null|object    $class    Give the class where to constant should be found or as default it's $this
	 *
	 * @return mixed
	 * @since   3.0.0 First time introduced.
	 */
	public function getClassConstant(string $name, object $class = NULL) : mixed;
	
	/**
	 * @return null|RegisterInterface
	 * @since   3.0.0 First time introduced.
	 */
	public function getRegisterApplication() : ?RegisterInterface;
	
	/**
	 * @param    RegisterInterface    $register
	 *
	 * @return void
	 * @since   3.0.0 First time introduced.
	 */
	public function setRegisterApplication(RegisterInterface $register) : void;
}
