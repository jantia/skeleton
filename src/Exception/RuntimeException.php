<?php

/**
 * Jantia Platform
 *
 * @package        Jantia/Skeleton
 * @license        BSD-3-Clause
 */
declare( strict_types=1 );

//
namespace Jantia\Skeleton\Exception;

/**
 * @version 3.0.0
 * @since   3.0.0 First time introduced.
 */
class RuntimeException extends \RuntimeException implements ExceptionInterface {

}
